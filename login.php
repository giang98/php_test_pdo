<?php 
        
    //     $username = isset($_POST['username']) ? $_POST['username'] : null;
    //     $password = isset($_POST['password']) ? $_POST['password'] : null;

    // if($_SERVER['REQUEST_METHOD'] == 'POST') {
    //     session_start(); 

    //     $error = [];

    //     if(empty($username)) {
    //         $error['username'] = 'Tên đăng nhập không được để trống!';
    //     }

    //     if(empty($password)) {
    //         $error['password'] = 'Nhập mật khẩu!';
    //     }

    //     function checkUser($username, $password){
    //         if(($username == 'admin') && ($password == '12345')) {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
        
    //     if(checkUser($username, $password)) {
    //         $_SESSION['username'] = $username;
    //         header('Location: create.php');
    //     } else {
    //         $error['password'] = 'Tên đăng nhập hoặc mật khẩu không đúng!';
    //         // echo 'Tên đăng nhập hoặc mật khẩu không đúng!';
    //     }

        
    // }

?>

<?php
    $servername = 'localhost';
    $user = 'root';
    $pass = '';
    $dbname = 'crud_pdo';
           
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;


    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $error = [];

            if(empty($username)) {
                $error['username'] = 'Tên đăng nhập không được để trống!';
            }

            if(empty($password)) {
                $error['password'] = 'Nhập mật khẩu!';
            }

            $sql = "SELECT username, password FROM users WHERE username = ? AND password = MD5(?) LIMIT 1" ;

            if(!isset($_COOKIE["user_login"])) {
                $sql .= " AND password = " . md5($_POST['password']) . "";
            }

            $statement = $conn->prepare($sql);

            $statement->execute([$username, $password]);

            $user = $statement->fetch();           
            

            if($user) {                
                header('Location: create.php');

                if(!empty($_POST["remember"])) {
                    setcookie ("user_login",$_POST["member_name"],time()+ (3 * 24 * 60 * 60));

                } else {
                    if(isset($_COOKIE["user_login"])) {
                        setcookie ("user_login","");
                    }
                }

            } else {
                $error['password'] = 'Tên đăng nhập hoặc mật khẩu không đúng!';
                // echo 'Tên đăng nhập hoặc mật khẩu không đúng!';
            }
            
        } 

    } catch(PDOException $e) {
        echo "Connection failed" .$e->getMessage();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <div class="container">
        <form class="phuongtrinh" method="POST">
            <h3>Đăng nhập</h3>

            <div class="gr-form">
                <p>Tên đăng nhập</p>
                <input type="text" name="username" value="<?php echo $username ?>">

                <?php if(isset($error['username'])) { ?>
                    <small style="color: red"> <?php echo $error['username'] ?></small>
                <?php } ?>
            </div>

            <div class="gr-form">
                <p>Mật khẩu</p>
                <input type="password" name="password" value="<?php echo $password ?>" >

                <?php if(isset($error['password'])) { ?>
                    <small style="color: red"> <?php echo $error['password'] ?></small>
                <?php } ?>
            </div>
            
            <div class="gr__remember">
                <input type="checkbox" id="remember" name="remember" <?php if(isset($_COOKIE["user_login"])) { ?> checked <?php } ?> />
                <label>Remember</label><br>
            </div>

            <div class="clear-float"></div>

            <button name="next">Đăng nhập</button>

        </form>
        
    </div>
</body>
</html>