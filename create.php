<?php 

    // $id = isset($_POST['id']) ? $_POST['id'] : null ;
    $fullname = isset($_POST['fullname']) ? $_POST['fullname'] : null ; 
    $dob = isset($_POST['dob']) ? $_POST['dob'] : null ;
    $hometownn = isset($_POST['hometownn']) ? $_POST['hometownn'] : null ;
    $phone_number = isset($_POST['phone_number']) ? $_POST['phone_number'] : null ;

    $servername = 'localhost';
    $user = 'root';
    $pass = '';
    $dbname = 'crud_pdo';


    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        if (isset($_POST['save'])) {
            
            // $student = [
            //     // 'id' => $id,
            //     'fullname' => $fullname,
            //     'dob' => $dob,
            //     'hometownn' => $hometownn,
            //     'phone_number' => $phone_number
            // ];

            try {
                $conn = new PDO("mysql:host=$servername, dbname=$dbname", $user, $pass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);        

                $stmt = $conn->prepare("INSERT INTO students (fullname, dob, hometown, phonenumber) VALUES (?, ?, ?, ?)");

                // $stmt->bindParam(1, $id);
                $stmt->bindParam(1, $fullname);
                $stmt->bindParam(2, $dob);
                $stmt->bindParam(3, $hometown);
                $stmt->bindParam(4, $phone_number);

                $stmt->execute();
                // $stmt->fetch();

                // while ($obj = $stmt->fetch()) {
                //     echo $obj->$fullname;
                //     echo $obj->$dob;
                //     echo $obj->$hometown;
                //     echo $obj->$phone_number;
                // }

                //print_r($result);

            } catch (PDOException $e) {
                echo "connection failed" .$e->getMessage();
            }

        }

        $errors = [];


        if(!preg_match('/^.{2,30}$/' , $fullname)) {
            $errors['fullname'] = 'Họ tên phải lớn hơn 2 kí tự!';
        }

        if(empty($dob)) {
            $errors['dob'] = 'Ngày sinh không được để trống!';
        }

        if(empty($hometownn)) {
            $errors['hometownn'] = 'Địa chỉ không được để trống!';
        }

        if(!preg_match('/^.{8,12}$/' , $phone_number)) {
            $errors['phone_number'] = 'Số điện thoại không đúng!';
        }

    
    }
     

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>

    <div class="container">
        <form class="signin" method="POST">
            <h3>Nhập thông tin</h3>

            <div class="gr-form">
                <p>Họ tên :</p>
                <input type="text" name="fullname" value="<?php echo $fullname ?>">
                <?php if(isset($errors['fullname'])) { ?>
                        <small style="color: red"><?php echo $errors['fullname'] ?></small>
                    <?php } ?>
            </div>

            <div class="gr-form">
                <p>Năm sinh :</p>
                <input type="text" name="dob" value="<?php echo $dob ?>">
                <?php if(isset($errors['dob'])) { ?>
                        <small style="color: red"><?php echo $errors['dob'] ?></small>
                    <?php } ?>
            </div>

            <div class="gr-form">
                <p>Quê quán :</p>
                <input type="text" name="hometownn" value="<?php echo $hometownn ?>">
                <?php if(isset($errors['hometownn'])) { ?>
                        <small style="color: red"><?php echo $errors['hometownn'] ?></small>
                    <?php } ?>
            </div>

            <div class="gr-form">
                <p>Số điện thoại :</p>
                <input type="text" name="phone_number" value="<?php echo $phone_number ?>">
                <?php if(isset($errors['phone_number'])) { ?>
                        <small style="color: red"><?php echo $errors['phone_number'] ?></small>
                    <?php } ?>
            </div>

            <button name="save">Lưu thông tin</button>

        </form>


        <table border ='1' >
            <tr>
                <th>STT</th>
                <th>Mã Sinh viên</th>
                <th>Họ tên</th>
                <th>Năm sinh</th>
                <th>Quê quán</th>
                <th>Số điện thoại</th>
                <th>Sửa</th>
                <th>Xóa</th>
            </tr>

              
            <?php
            // print_r($students);

            try {
                
                $conn = new PDO("mysql:host=$servername, dbname=$dbname", $user, $pass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare('SELECT * FROM students');
                $stmt ->setFetchMode(PDO::FETCH_ASSOC);

                $stmt->execute();
                $result = $stmt->fetchAll();
                
               foreach($result as $student) { ?>

                    <tr> 

                        <td><?php echo $student['id'] ?? null ?></td>

                        <td><?php echo $student['fullname'] ?? null ?></td>

                        <td> <?php echo $student['dob'] ?? null ?> </td>

                        <td> <?php echo $student['hometown'] ?? null ?> </td>

                        <td> <?php echo $student['phone_number'] ?? null ?> </td>

                        <td><a href="edit.php?key=<?php echo $key; ?>">Sửa</a></td>

                        <td><a href="delete.php?key=<?php echo $key; ?>">Xóa</a></td>

                    </tr>

            <?php } 

            } catch (PDOException $e) {
                echo 'Connection failed'.$e->getMessage();
            }
           
            /*foreach($students as $key => $student) {  ?>
             
                <tr> 
                    <td><?php echo ($key+1) ?></td>

                    <td><?php echo $student['id'] ?? null ?></td>

                    <td><?php echo $student['fullname'] ?? null ?></td>

                    <td> <?php echo $student['dob'] ?? null ?> </td>

                    <td> <?php echo $student['hometownn'] ?? null ?> </td>

                    <td> <?php echo $student['phone_number'] ?? null ?> </td>

                    <td><a href="edit.php?key=<?php echo $key; ?>">Sửa</a></td>

                    <td><a href="delete.php?key=<?php echo $key; ?>">Xóa</a></td>

                </tr>

            <?php  } ?>*/

                    ?>       
        </table>

    </div>
</body>
</html>