<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validate-form</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>

    <?php

        $name = isset($_POST['name']) ? $_POST['name'] : null;
        $email = isset($_POST['email']) ? ($_POST['email']) : null;
        $password1 = isset($_POST['password1']) ? ($_POST['password1']) : null;
        $password2 = isset($_POST['password2']) ? ($_POST['password2']) : null;
        $phone_number = isset($_POST['phone_number']) ? ($_POST['phone_number']) : null;
        $address = isset($_POST['address']) ? ($_POST['address']) : null;


       

        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            $errors = [];

            // if(empty($name)) {
            //     $errors['name'] = 'Name is not empty';
            // }elseif ( (count($name) < 2) || (count($name) > 30) ) {
            //     $errors['name'] = 'Name is wrong' ;
            // }

            if(!preg_match('/^.{2,30}$/', $name)) {
                $errors['name'] = 'Name is wrong!';
            }

            if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email) {
                $errors['email'] = 'Email is not empty' ; 
            }

            
            if(!preg_match( '/^(?=.*[w])(?=.*\d)[w\d]{4,}$/', $password1)) {
                $errors['password1'] = 'Password is not empty, larger than 4 chars' ;
            }

            // if (empty($password1)) {
            //     $errors['password1'] = 'Password is not empty' ;
            // } elseif ($password1 < 4) {
            //     $errors['password1'] = 'Password is too short' ;
            // }

            if ($password1 != $password2){
                $errors['password2'] = 'Password is not match' ;
            } 

            if(!preg_match ('/^.{8, 12}$/', $phone_number)) {
                $errors['phone_number'] = 'Phone Number is wrong' ;
            }
            
            if(!preg_match ('/^.{5, 120}$/', $address)) {
                $errors['address'] = 'Address is wrong' ;
            }
        }

    ?>

    <div class="page">
        <form class="signin" method="post" >
            <h3>Đăng kí tài khoản</h3>
            
            <div class="container">
                <div class="gr-form">
                    <label>Tên</label>
                    <input type="text" name="name" value="<?php echo $name; ?>">
                    <?php if(isset($errors['name'])) { ?>
                        <small style="color: red"><?php echo $errors['name'] ?></small>
                    <?php } ?>
                </div>

                <div class="gr-form">
                    <label>Email</label>
                    <input type="text" name="email" value="<?php echo $email; ?>">                   
                    <?php if(isset($errors['email'])) { ?>
                        <small style="color: red"><?php echo $errors['email'] ?></small>
                    <?php } ?>
                </div>

                <div class="gr-form">
                    <label>Mật khẩu</label>
                    <input type="text" name="password1" value="<?php echo $password1; ?>">
                    <?php if(isset($errors['password1'])) { ?>
                        <small style="color: red"><?php echo $errors['password1'] ?></small>
                    <?php } ?>
                </div>

                <div class="gr-form">
                    <label>Xác nhận mật khẩu</label>
                    <input type="text" name="password2" value="<?php echo $password2; ?>">
                    <?php if(isset($errors['password2'])) { ?>
                        <small style="color: red"><?php echo $errors['password2'] ?></small>
                    <?php } ?>
                </div>

                <div class="gr-form">
                    <label>Số điện thoại</label>
                    <input type="text" name="phone_number" value="<?php echo $phone_number; ?>">
                    <?php if(isset($errors['phone_number'])) { ?>
                        <small style="color: red"><?php echo $errors['phone_number'] ?></small>
                    <?php } ?>
                </div>

                <div class="gr-form">
                    <label>Nhập địa chỉ</label>
                    <input type="text" name="address" value="<?php echo $address; ?>">
                    <?php if(isset($errors['address'])) { ?>
                        <small style="color: red"><?php echo $errors['address'] ?></small>
                    <?php } ?>
                </div>

                <button type="submit" name="submit_button">Tạo tài khoản</button>

            </div>
        </form>                  
    </div>
</body>
</html>

