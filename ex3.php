<?php
    $districts = [
        [
            'code' => '001',
            'type' => 'quận',
            'name' => 'Quận Ba Đình'
        ],
        [
            'code' => '002',
            'type' => 'quận',
            'name' => 'Quận Tây Hồ'
        ],
        [
            'code' => '003',
            'type' => 'quận',
            'name' => 'Quận Tây Hồ'
        ],
        [
            'code' => '004',
            'type' => 'quận',
            'name' => 'Quận Long Biên'
        ],
        [
            'code' => '005',
            'type' => 'quận',
            'name' => 'Quận Cầu Giấy'
        ],
        [
            'code' => '006',
            'type' => 'quận',
            'name' => 'Quận Đống Đa'
        ],
        [
            'code' => '007',
            'type' => 'quận',
            'name' => 'Quận Hai Bà Trưng'
        ],
        [
            'code' => '008',
            'type' => 'quận',
            'name' => 'Quận Hoàng Mai'
        ],
        [
            'code' => '009',
            'type' => 'quận',
            'name' => 'Quận Thanh Xuân'
        ],
        [
            'code' => '016',
            'type' => 'huyện',
            'name' => 'Huyện Sóc Sơn'
        ],
        [
            'code' => '017',
            'type' => 'huyện',
            'name' => 'Huyện Đông Anh'
        ],
        [
            'code' => '018',
            'type' => 'huyện',
            'name' => 'Huyện Gia Lâm'
        ]

    ]

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>District</title>
    <link rel="stylesheet" href="./css/style.css">

</head>
<body>
    <table border = '1'>
        <tr>
            <th>code</th>
            <th>type</th>
            <th>name</th>
        </tr>

        <?php 
            foreach ($districts as $key => $district) { ?>
            <tr>
                <td><?php echo $district['code'] ?></td>
                <td class="quan"><?php echo $district['type'] ?></td>
                <td><?php echo $district['name'] ?></td>
            </tr>

            <?php 
                if($district['type'] == 'huyen') { ?>
                    <style>
                        .quan {
                            font-weight: italic;
                        }
                    </style>
            <?php } ?>
        <?php  } ?>       


    </table>
</body>
</html>