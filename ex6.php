<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <?php
        $a = isset($_POST['a']) ? $_POST['a'] : null;
        $b = isset($_POST['b']) ? $_POST['b'] : null;
        $c = isset($_POST['c']) ? $_POST['c'] : null;


        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $error = [];
            $data = [];

            // if(empty($a)) {
            //     $error['a'] = 'Not empty!' ;
            // }
    
            // if(empty($b)) {
            //     $error['b'] = 'Not empty!' ;
            // }
    
            // if(empty($c)) {
            //     $error['c'] = 'Not empty!' ;
            // }
    
            if ($a == 0 && $b == 0 ) {
                // echo 'Phương trình vô nghiệm';
                $error['result'] = 'Phương trình vô nghiệm';
            } 

            elseif($a == 0) {
                // echo 'Phương trình có 1 nghiệm x = ';

                $x = -$c/ $b ;
                $data['text'] = "Phương trình có nghiệm x = \n";
                $data['result'] = [$x];
            }


            else {
                $delta = $b*$b - 4*$a*$c ;

                if($delta < 0) {
                    $error['result'] = 'Phương trình vô nghiệm';
                }

                elseif ($delta == 0) {
                    // echo 'Phương trình có nghiệm x =  ';
                    $x = -$b/ 2*$a;

                    $data['text'] = "Phương trình có nghiệm x =  \n";
                    $data['result'] = [$x];
                }

                else {
                    // echo 'Phương trình có 2 nghiệm phân biệt: ';
                    
                    $x1 = (-$b + sqrt($delta)) / 2*$a;
                    $x2 = (-$b - sqrt($delta)) / 2*$a;

                    $data['text'] = "Phương trình có 2 nghiệm phân biệt x1, x2 : \n";                       

                    $data['result'] = [$x1, $x2];
                }
            }
    
        }

    ?>


    <div class="page">
        <div class="container">
            <form class="phuongtrinh" method="post">
                <div class="gr-form">
                    <p>A: </p>
                    <input type="text" name="a" value="<?php echo $a ?>">
                </div>

                <div class="gr-form">
                    <p>B: </p>
                    <input type="text" name="b" value="<?php echo $b ?>">
                </div>

                <div class="gr-form">
                    <p>C: </p>
                    <input type="text" name="c" value="<?php echo $c ?>">
                </div>

                <button name="result">Result</button>

                <?php if( isset ($data['result'] )) { ?>

                <div style="color: lightseagreen; margin: 10px">
                    <?php
                        echo $data['text'];
                        print_r ( $data['result']) ;
                     ?>
                </div>

                <?php } else { ?>
                    <div style="margin: 10px">
                        <?php echo $error['result']; ?>
                    </div>
                    
                <?php } ?>

            </form>
        </div>
    </div>
</body>
</html>