<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test</title>
</head>
<body>
    <form method="post" action="test3.php">
        Name: <input type="text" name="fname">;
        Password: <input type="text" name="fpassword">;
        <input type="submit">
    </form>
</body>
</html>

<?php 

    date_default_timezone_set('Asia/Ho_Chi_Minh');
    echo 'Today : ';
    echo date('d/m/Y - H:i:s');
    echo "<br>";

    $h = date('H');
    $m = date('i');
    $s = date('s');

    $dateint = mktime($h, $m, $s,  date("m"), date("d")-3,   date("Y"));
    echo '3days ago : ';
    echo date('d/m/Y - H:i:s', $dateint);
    echo "<br>";

    // $day = mktime($h, $m, $s, 1 date("m"), date("d")+3,   date("Y"));
    // echo '3days later: ';
    // echo date('d/m/Y - H:i:s', $day);
    // echo "<br>";

    $nextmonth = mktime( $h, $m, $s, date("m")+1, date("d"),   date("Y"));
    echo 'next momth: ';
    echo date('d/m/Y - H:i:s', $nextmonth);
    echo "<br>";

    $nextyear = mktime( $h, $m, $s, date("m"), date("d"), date("Y")+1 );
    echo 'next year: ';
    echo date('d/m/Y - H:i:s', $nextyear);
    echo "<br>";

    $sametime = mktime($h, $m, $s, 9, 20, date("Y")-1);
    echo '20/9/ 1year ago : ';
    echo date('d/m/Y - H:i:s', $sametime);
    echo "<br>";

?>