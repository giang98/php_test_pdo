<?php
    $users = array(
        array(
            'name' => 'Shouta Yamamoto',
            'site' => 'kaiyouit.com',
            'position' => 'founder'
        ),
        array(
            'name' => 'Nguyễn Huy Hùng',
            'site' => 'kaiyouit.com',
            'position' => 'founder'
        ),
        array(
            'name' => 'Nguyễn Hữu Long',
            'site' => 'kaiyouit.com',
            'position' => 'PM'
        ),
        array(
            'name' => 'Đỗ Như Tuấn',
            'site' => 'kaiyouit.com',
            'position' => 'leader'
        )
    );

    $search = isset($_GET['search']) ? $_GET['search'] : null;

    if($search != null) {
        // $newUsers = [];

        // foreach($users as $user) {
        //     if(strpos($user['name'], $search) !== false) {
        //         $newUsers[] = $user;
        //     }
        // }

        // $users = $newUsers;

        $users = array_filter($users, function($user) use ($search) { // use: sử dụng biến ngoài function.
            return strpos($user['name'], $search) !== false; // strpos: ktra chuỗi trong chuỗi.
        });
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test</title>
</head>
<body>
    <form class="search" method="GET">
        <input type="text" name="search" value="<?php echo $search ?>">
        <button>Search</button>
    </form>

    <table border = '1'>
        <tr>
            <th>STT</th>
            <th>Name</th>
            <th>Site</th>
            <th>Position</th>
        </tr>

        <?php 
            foreach ($users as $key => $user) { ?>
            <tr>
                <td><?php echo ($key + 1) ?></td>
                <td><?php echo $user['name'] ?></td>
                <td><?php echo $user['site'] ?></td>
                <td><?php echo $user['position'] ?></td>
            </tr>
        <?php  } ?>
                
    </table>

</body>
</html>