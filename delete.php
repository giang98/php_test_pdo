<?php
    $key = $_GET['key'] ?? null;

    if ($key !== null) {
        // session_start();
         
        // $students = $_SESSION['students'] ?? [] ;

        // unset($students[$key]);

        // $_SESSION['students'] = $students;

        $servsername = 'localhost';
        $dbname = 'crud_pdo';
        $username = 'root';
        $password = '';

        try {
            $conn = new PDO("mysql:host=$servsername, dbname=$dbname", $username, $password);
            $conn->setAttribute(PDO::AUTO_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "DELETE FROM students WHERE id=$key";
            $conn->exec($sql);
            
        } catch (PDOException $e) {
            echo 'Connection failed' .$e->getMessage();
        }

        header('Location: create.php');
    }
    
?>