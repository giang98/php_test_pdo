
<?php
    $servername = 'localhost';
    $user = 'root';
    $pass = '';
    $dbname = 'crud_pdo';
           
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $phone_number = isset($_POST['phone_number']) ? $_POST['phone_number'] : null;

    $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    function randString($length, $charset)
    {
        $str = '';
        $count = strlen($charset);
        for($i = 0; $i < $length; $i++) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
        return $str;
    }


    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $error = [];

            if(empty($username)) {
                $error['username'] = 'Tên đăng nhập không được để trống!';
            }

            if(empty($phone_number)) {
                $error['phone_number'] = 'Nhập số điện thoại!';
            }

            $sql = "SELECT password FROM users WHERE username = ? AND phone_number = ? " ;

            $statement = $conn->prepare($sql);

            $statement->execute([$username, $phone_number]);

            $user = $statement->fetch();

            if($user) {                
                // header('Location: create.php');
                // echo "Password changed!";
                
                $random_string = randString(6, $charset);
                // echo 'Mật khẩu mới: '.$random_string;

                $sql = "UPDATE users SET password = MD5(?) WHERE username = ? AND phone_number = ? ";
                $statement = $conn->prepare($sql);

                $statement->execute([$random_string, $username, $phone_number]);

            } else {
                $error['phone_number'] = 'Tên đăng nhập hoặc số điện thoại không đúng!';
                // echo 'Tên đăng nhập hoặc mật khẩu không đúng!';
            }
            
        }

    } catch(PDOException $e) {
        echo "Connection failed" .$e->getMessage();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <div class="container">
        <form class="phuongtrinh" method="POST">
            <h3>Đăng nhập</h3>

            <div class="gr-form">
                 <!-- <p>Mật khẩu mới: </p> -->
                <?php 
                // echo "Password changed! <br>". $random_string; 
                if($user) {                
                    // header('Location: create.php');
                    echo "Password changed!";
                    
                    $random_string = randString(6, $charset);
                    echo 'Mật khẩu mới: '.$random_string;
    
                    $sql = "UPDATE users SET password = MD5(?) WHERE username = ? AND phone_number = ? ";
                    $statement = $conn->prepare($sql);
    
                    $statement->execute([$random_string, $username, $phone_number]);
    
                }
                ?>

                <p>Tên đăng nhập</p>               

                <input type="text" name="username" value="<?php echo $username ?>">

                <?php if(isset($error['username'])) { ?>
                    <small style="color: red"> <?php echo $error['username'] ?></small>
                <?php } ?>
            </div>

            <div class="gr-form">
                <p>Số điện thoại</p>
                <input type="phone_number" name="phone_number" value="<?php echo $phone_number ?>" >

                <?php if(isset($error['phone_number'])) { ?>
                    <small style="color: red"> <?php echo $error['phone_number'] ?></small>
                <?php } ?>
            </div>

            <button name="next">Check</button>

        </form>
        
    </div>
</body>
</html>


